﻿using Limefy.Service.Default;
using Limefy.Service.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Limefy.Web.Data;
using Limefy.Web.Data.Models;

namespace Limefy.Web.Services
{
    public class EventService
    {
        private readonly IRepository<SocialEventData> _repoSocialEventData;
        private readonly Container _service = new Container(new Uri("http://ligoserviceeurope.azurewebsites.net"));


        public EventService(IRepository<SocialEventData> repoSocialEventData)
        {
            _repoSocialEventData = repoSocialEventData;
        }


        public async Task<SocialEvent> CreateEventAsync()
        {

            // create event in service
            var socialEvent = new SocialEvent();
            socialEvent.Code = Guid.NewGuid().ToString();
            socialEvent.Name = "New event";
            _service.AddToSocialEvents(socialEvent);
            await _service.SaveChangesAsync(Microsoft.OData.Client.SaveChangesOptions.None);


            //var eventData = new SocialEventData
            //{
            //    LigoId = socialEvent.Id,
            //    EventCode = socialEvent.Code
            //};

            //_repoSocialEventData.Add(eventData);
            //_repoSocialEventData.SaveChanges();

            return socialEvent;
        }

        public async Task<SocialEvent> GetEventByCodeAsync(string code)
        {
            var query = _service.SocialEvents.GetByCode(code);
            return await query.GetValueAsync();
        }

        public async Task<IEnumerable<Category>> GetEventCategories(long ligoEventId)
        {
            var query = _service.Categories.GetByParent(ligoEventId, 0);
            return await query.GetAllPagesAsync();
        }

        public async Task<IEnumerable<SocialEvent>> GetAllEvents()
        {
            return await _service.SocialEvents.GetAllPagesAsync();
        }

        public async Task<IEnumerable<SubCategory>> GetCategorySubcategoies(Category cat)
        {
            var query = _service.SubCategories.GetByHint(cat.EventId, cat.Id, 1, "");
            return await query.GetAllPagesAsync();
        }
    }
}
