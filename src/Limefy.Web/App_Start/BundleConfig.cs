﻿using System.Web;
using System.Web.Optimization;

namespace Limefy
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/mainjs").Include(
                "~/" + Links.Scripts.jquery_jcarousel_js,
                "~/" + Links.Scripts.jcarousel_data_attributes_js,
                "~/" + Links.Scripts.main_js
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/" + Links.Content.css.all_css,
                      "~/" + Links.Content.css.jcarousel_data_attributes_css
                      ));
        }
    }
}
