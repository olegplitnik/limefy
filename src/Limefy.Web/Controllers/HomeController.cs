﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using Limefy.Service.WebService.Models;
using Limefy.Web.Models;
using Limefy.Web.Services;

namespace Limefy.Web.Controllers
{
    public partial class HomeController : Controller
    {
        private readonly EventService _eventService;


        public HomeController(EventService eventService)
        {
            _eventService = eventService;
        }

        public virtual async Task<ActionResult> Index()
        {
            var events = await _eventService.GetAllEvents();
            
            var model = new EventDataListModel
            {
                SocialEvents = events
            };

            return View(model);
        }


    }
}
