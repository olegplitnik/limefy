﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Limefy.Service.WebService.Models;
using Limefy.Web.Services;
using Limefy.Web.Models;

namespace Limefy.Web.Controllers
{
    public partial class EventController : Controller
    {
        private readonly EventService _eventService;

        public EventController(EventService eventService)
        {
            _eventService = eventService;
        }

        public virtual async Task<ActionResult> Create()
        {
            var @event = await _eventService.CreateEventAsync();

            var model = new EventDataModel
            {
                SocialEvent = @event,
                EventCategories = new List<Category>(),
                Subcategories = new List<SubCategory>()
            };

            return View(MVC.Event.Views.Edit, model);
        }


        public virtual async Task<ActionResult> Edit(string id)
        {
            var ev = await _eventService.GetEventByCodeAsync(id);
            var cats = await _eventService.GetEventCategories(ev.Id);

            var eventCategories = cats as IList<Category> ?? cats.ToList();


            var tasks = from c in eventCategories
                        select _eventService.GetCategorySubcategoies(c);


            var subCats = await Task.WhenAll(tasks);


            var model = new EventDataModel
            {
                SocialEvent = ev,
                EventCategories = eventCategories,
                Subcategories = subCats.SelectMany(c => c).ToList()
            };

            return View(model);
        }


    }
}
