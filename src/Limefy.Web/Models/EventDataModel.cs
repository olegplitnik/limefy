﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Limefy.Service.WebService.Models;

namespace Limefy.Web.Models
{
    public class EventDataModel
    {

        public SocialEvent SocialEvent { get; set; }

        public IEnumerable<Category> EventCategories { get; set; }

        public List<SubCategory> Subcategories { get; set; }

    }


    public class EventDataListModel
    {
        public IEnumerable<SocialEvent> SocialEvents { get; set; }

    }

}