﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limefy.Web.Data
{
    public class EfConfiguration : DbConfiguration
    {
        public EfConfiguration()
        {

            this.SetDefaultConnectionFactory(new SqlConnectionFactory("DefaultConnection"));
        }
    }
}
