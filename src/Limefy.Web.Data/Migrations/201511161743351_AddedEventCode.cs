namespace Limefy.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedEventCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SocialEventData", "EventCode", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SocialEventData", "EventCode");
        }
    }
}
