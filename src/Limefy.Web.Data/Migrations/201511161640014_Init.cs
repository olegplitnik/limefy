namespace Limefy.Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SocialEventData",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LigoId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SocialEventData");
        }
    }
}
