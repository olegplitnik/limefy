﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limefy.Web.Data.Models
{
    public class SocialEventData
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public long LigoId { get; set; }

        [Required]
        public string EventCode { get; set; }
    }
}
